<?php

use App\Client;
use Faker\Generator as Faker;
use App\Room;

$factory->define(App\Reservation::class, function (Faker $faker) {
    $dateIn = $faker->date('2018-m-d');
    $dateOut = date('2018-m-d', strtotime($dateIn . '+5 days'));
    return [
        'date_in' => $dateIn,
        'date_out' => $dateOut,
        'client_id' => Client::all()[rand(1, 4)]->id,
        'room_id' => Room::all()[rand(1, 4)]->id
    ];
});
