<?php

use Faker\Generator as Faker;

$factory->define(App\Room::class, function (Faker $faker) {
    $roomName = rand(1, 3);
    return [
        'name' => $roomName,
        'floor' => rand(1, 2),
        'description' => $roomName
    ];
});
