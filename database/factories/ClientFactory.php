<?php

use Faker\Generator as Faker;

$factory->define(App\Client::class, function (Faker $faker) {
    return [
        'title' => $faker->title,
        'name' => $faker->name,
        'last_name' => $faker->lastName,
        'address' => $faker->address,
        'zip_code' => $faker->postcode,
        'city' => $faker->city,
        'state' => $faker->country,
        'email' => $faker->unique()->safeEmail,
    ];
});
