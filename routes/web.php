<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'ContentsController@index')->name('home');

Route::middleware('auth')->group(function () {
    Route::get('/clients', 'ClientController@index')->name('clients');

    Route::get('/upload', 'ContentsController@imgUpload');

    Route::post('/contents/upload', 'ContentsController@upload');

    Route::get('/clients/export', 'ClientController@export');
    
    Route::get('/clients/new', 'ClientController@create')->name('new_client');

    Route::post('/clients', 'ClientController@store')->name('create_client');

    Route::get('/clients/{client}', 'ClientController@edit')->name('show_client');
    
    Route::post('/clients/{client}', 'ClientController@update')->name('update_client');
    
    // available rooms
    Route::get('/reservations/{client}', 'RoomController@show')->name('check_room');
    
    // get available rooms
    Route::post('/reservations/{client}', 'RoomController@edit');
    
    // book room for the client
    Route::get('/book/rooms/{client}/{room}/{date_in}/{date_out}', 'ReservationController@store')->name('book_room');
});

Auth::routes();

// Route::get('/passwords/generate', function () {
//     return bcrypt('123456789');
// });

// Route::get('/about', function () {
//     $res_arr = [];
//     $res_arr['author']  = 'men3m';
//     $res_arr['version'] = '0.1.1';

//     return $res_arr;
//     // return '<h3>About</h3>';
// });

// Route::get('/home', function () {
//     $data = [];
//     $data['version'] = '0.1.1';

//     return view('welcome', $data);
// });


// Route::get('/di', 'ClientController@di');

// Route::get('/facades/db', function () {
//     return DB::select('show databases');
// });

// Route::get('/facades/encrypt', function () {
//     return Crypt::encrypt('123456789');
// });

// Route::get('/facades/decrypt', function () {
//     return Crypt::decrypt('eyJpdiI6Im5IM1poWlhpanV6dHdmcDZkM3ZXZGc9PSIsInZhbHVlIjoiS3lzNUJFTTJNS1ZPM1JBNTRoeFFJTEoxQSsxY215SHJTQStDUys1SHY5ST0iLCJtYWMiOiJkMzA1N2M5MzYzMGE0MDg2NmVkMTIwZDk2YWM3NzI1MWE0MjQxYTFkNTFhNTkwMTU5NzQ1ZjQ0MzA2ZTRiM2Q3In0=');
// });

// Route::get('/home', 'HomeController@index')->name('home');
