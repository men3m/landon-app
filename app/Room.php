<?php

namespace App;

use App\Model;
use Illuminate\Support\Facades\DB;

class Room extends Model
{
    public function available($date_in, $date_out)
    {
        return DB::table('rooms as r')
                        ->select('r.id', 'r.name')
                        ->whereRaw("r.id NOT IN (
                            SELECT b.room_id FROM reservations b
                            WHERE NOT(b.date_out < '{$date_in}' OR b.date_in > '{$date_out}'))")
                            ->orderBy('r.id')
                            ->get();
    }

    public function isBooked($date_in, $date_out)
    {
        return DB::table('reservations')
                        ->whereRaw("NOT(date_out < '{$date_in}' OR date_in > '{$date_out}')")
                        ->where('room_id', $this->id)->count();
    }
}
