<?php

namespace App\Http\Controllers;

use App\Title;
use Exception;
use App\Client;
use Illuminate\Http\Request;

class ClientController extends Controller
{
    public function __construct(Title $titles)
    {
        $this->titles = $titles->all();
        // $this->client = $client;
    }

    /**
     * Exprt all clients in excel format.
     *
     * @return \Illuminate\Http\Response
     */
    public function export()
    {
        $clients = Client::all();
        header('Content-Disposition: attachment; filename=export.xls');
        return view('clients.export', compact('clients'));
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $clients = Client::all();
        return view('clients.index', compact('clients'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data['titles'] = $this->titles;
        $data['modify'] = 0;
        return view('clients.new', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate(request(), [
            'title'     => 'required',
            'name'      => 'required|min:2',
            'last_name' => 'required|min:2',
            'address'   => 'required',
            'zip_code'  => 'required',
            'city'      => 'required',
            'state'     => 'required',
            'email'     => 'required'
        ]);

        $client = new Client(request([
            'title'    ,
            'name'     ,
            'last_name',
            'address'  ,
            'zip_code' ,
            'city'     ,
            'state'    ,
            'email'
        ]));
        
        // try {
        $client->save();
        // } catch (Exception $e) {
        //     abort(500, $e->getMessage());
        // }

        session()->flash('message', 'Client has been saved!');

        return redirect('/clients');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Client $client)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Client $client)
    {
        $data['titles'] = $this->titles;
        $data['modify'] = 1;

        return view('clients/new', $data, compact('client'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $client)
    {
        $this->validate(request(), [
            'title'     => 'required',
            'name'      => 'required|min:2',
            'last_name' => 'required|min:2',
            'address'   => 'required',
            'zip_code'  => 'required',
            'city'      => 'required',
            'state'     => 'required',
            'email'     => 'required'
        ]);

        $co = json_decode($client);
        $client_data = Client::find($co->id);

        $client_data->title = $request->input('title');
        $client_data->name = $request->input('name');
        $client_data->last_name = $request->input('last_name');
        $client_data->address = $request->input('address');
        $client_data->zip_code = $request->input('zip_code');
        $client_data->city = $request->input('city');
        $client_data->state = $request->input('state');
        $client_data->email = $request->input('email');
        
        $client_data->save();

        session()->flash('message', 'Client has been published!');

        return redirect('clients');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
