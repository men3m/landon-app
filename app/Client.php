<?php

namespace App;

use App\Model;
use App\Reservation;

class Client extends Model
{
    public function reservations()
    {
        return $this->hasMany(Reservation::class);
    }
}
