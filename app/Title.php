<?php

namespace App;

class Title extends ReadOnlyBase
{
    protected $titles_array = ['Mr.', 'Mrs.', 'Ms.', 'Mx.', 'Dr.', 'Prof.'];
}
