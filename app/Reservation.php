<?php

namespace App;

use App\Room;
use App\Model;
use App\Client;

class Reservation extends Model
{
    public function client()
    {
        return $this->belongsTo(Client::class);
    }

    public function room()
    {
        return $this->belongsTo(Room::class);
    }
}
