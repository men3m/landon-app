<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;
use App\Title;

class ExampleTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testBasicTest()
    {
        $response = $this->get('/');

        $response->assertStatus(200);
    }

    // public function testTitleListInClients()
    // {
    //     $response = $this->get('/clients/new');
    //     $this->assertContains('Prof.', $response->getContent(), 'Should have Prof. title');
    // }
}
