<div>
    @if(isset($exception)))
        <small class="error">{{ $exception->getMessage() }}</small>
    @endif
</div>