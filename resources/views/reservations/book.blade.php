@extends('layouts.main')

@section('content')
    <div class="row">
        <div class="medium-12 large-12 columns">
            <h4>Clients/Booking</h4>
            <div class="medium-2  columns">BOOKING FOR:</div>
            <div class="medium-2  columns"><b>{{ $client->title }} {{ $client->name }} {{ $client->last_name }}</b></div>
            <form action="" method="POST">
                    {{ csrf_field() }}
                <div class="medium-1  columns">FROM:</div>
                <div class="medium-2  columns"><input name="dateFrom" value="{{ isset($date_in) ? $date_in : '' }}" type="text" class="datepicker" /></div>
                <div class="medium-1  columns">TO:</div>
                <div class="medium-2  columns"><input name="dateTo" value="{{ isset($date_out) ? $date_out : '' }}" type="text" class="datepicker" /></div>
                <div class="medium-2  columns"><input class="button" type="submit" value="SEARCH" /></div>
            </form>

            <table class="stack">
            <thead>
                <tr>
                <th width="200">Room</th>
                <th width="200">Availability</th>
                <th width="200">Action</th>
                </tr>
            </thead>
            <tbody>
                @unless(empty($date_in) || empty($date_out))
                    @foreach($rooms as $room)
                    <tr>
                        <td>{{ $room->name }}</td>
                        <td>
                            <div class="callout success">
                                <h7>Available</h7>
                            </div>
                        </td>
                        <td>
                            <a href="{{ route('book_room', [
                                                            'client' => $client->id,
                                                            'room' => $room->id,
                                                            'date_in' => $date_in,
                                                            'date_out' => $date_out
                                                            ]) }}" class="hollow button warning">BOOK NOW</a>
                        </td>
                    </tr>
                    @endforeach
                @endunless  
            </tbody>
            </table>
        </div>
    </div>
@endsection