@extends('layouts.main')

@section('content')
    <div class="row">
        <div class="medium-12 large-12 columns">
            <h4>{{ $modify ? 'Modify Client' : 'New Client' }}</h4>
            <form action="{{ $modify ? '/clients/' . $client : '/clients' }}" method="POST">

                {{ csrf_field() }}
            
                <div class="medium-4  columns">
                    <label>Title</label>
                    <select name="title">
                        @foreach($titles as $title)
                            <option value="{{ $title }}" {{ (isset($client->title) ? ($client->title == $title ? 'selected' : '') : '') }}>
                                {{ $title }}
                            </option>
                        @endforeach
                    </select>
                </div>
                <div class="medium-4  columns">
                    <label>Name</label>
                    <input name="name" type="text" value="{{ old('name') ? old('name') : (isset($client->name) ? $client->name : '') }}">
                    <small class="error">{{ $errors->first('name') }}</small>
                </div>
                <div class="medium-4  columns">
                    <label>Last Name</label>
                    <input name="last_name" type="text" value="{{ old('last_name') ? old('name') : (isset($client->last_name) ? $client->last_name : '') }}">
                    <small class="error">{{ $errors->first('last_name') }}</small>
                </div>
                <div class="medium-8  columns">
                    <label>Address</label>
                    <input name="address" type="text" value="{{ old('address') ? old('name') : (isset($client->address) ? $client->address : '') }}">
                    <small class="error">{{ $errors->first('address') }}</small>
                </div>
                <div class="medium-4  columns">
                    <label>zip_code</label>
                    <input name="zip_code" type="text" value="{{ old('zip_code') ? old('name') : (isset($client->zip_code) ? $client->zip_code : '') }}">
                    <small class="error">{{ $errors->first('zip_code') }}</small>
                </div>
                <div class="medium-4  columns">
                    <label>City</label>
                    <input name="city" type="text" value="{{ old('city') ? old('name') : (isset($client->city) ? $client->city : '') }}">
                    <small class="error">{{ $errors->first('city') }}</small>
                </div>
                <div class="medium-4  columns">
                    <label>State</label>
                    <input name="state" type="text" value="{{ old('state') ? old('name') : (isset($client->state) ? $client->state : '') }}">
                    <small class="error">{{ $errors->first('state') }}</small>
                </div>
                <div class="medium-12  columns">
                    <label>Email</label>
                    <input name="email" type="email" value="{{ old('email') ? old('name') : (isset($client->email) ? $client->email : '') }}">
                    <small class="error">{{ $errors->first('email') }}</small>
                </div>
                <div class="medium-12  columns">
                    <button class="button success hollow" type="submit">SAVE</button>
                </div>
            </form>

            {{-- @include('errors.500') --}}
            
        </div>
    </div>    
@endsection