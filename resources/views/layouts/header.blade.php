<!-- Start Top Bar -->
<div class="top-bar">
    <div class="row">
        <div class="top-bar-left">
        <ul class="dropdown menu" data-dropdown-menu="49qhhm-dropdown-menu" role="menubar">
            <li role="menuitem"><a href="/">Home</a></li>
            @auth
                <li role="menuitem"><a href="/clients">Clients</a></li>
                <li role="menuitem"><a href="#">Reservations</a></li>
            @endauth
        </ul>
        </div>
        @guest
            <div class="top-bar-right">
                <ul class="dropdown menu" data-dropdown-menu="49qhhm-dropdown-menu" role="menubar">
                    <li role="menuitem">
                        <a href="{{ route('login') }}">
                            Login
                        </a>
                    </li>
                </ul>
            </div>
        @else
            <div class="top-bar-right">
                <ul class="dropdown menu" data-dropdown-menu="49qhhm-dropdown-menu" role="menubar">
                    <li role="menuitem">
                        <a href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                            Logout
                        </a>

                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                            {{ csrf_field() }}
                        </form>
                    </li>
                </ul>
            </div>
        @endguest
    </div>
</div>
<!-- End Top Bar -->