@extends('layouts.main')

@section('content')
<div class="row">
    <div class="medium-6 large-5 columns">
        <form action="/contents/upload" method="POST" enctype="multipart/form-data">
            {{ csrf_field() }}
            <div>
                <input type="file" name="imgUpload" required>
                <small class="error">{{ $errors->first('imgUpload') }}</small>
            </div>
            <div>
                <button class="button success hollow" type="submit">Upload</button>
            </div>
        </form>
    </div>
</div>
@endsection